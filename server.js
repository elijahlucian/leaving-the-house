const express = require('express')
const serveIndex = require('serve-index')
const path = require('path')
const { filter, find } = require('lodash')

const app = express()
const port = process.env.PORT || 4242

const fs = require('fs')
const data = JSON.parse(fs.readFileSync('./data.json'))

const notFound = { error: 'not found' }

app.use(express.static(path.join(__dirname, './dist')))

app.get('/api/:model', ({ params: { model } }, res) => {
  const index = data[model]
  if (!index) res.send(notFound)
  res.send(index)
})

app.get('/api/essentials/:user_id', ({ params: { user_id } }, res) => {
  const err = { error: 'user not found' }
  res.send(
    find(data['essentials'], record => record.user_id == parseInt(user_id)) ||
      err
  )
})

app.get('/api/lists/:user_id', ({ params: { user_id } }, res) => {
  const err = { error: 'user not found' }
  res.send(
    filter(data['lists'], record => record.user_id == parseInt(user_id)) || err
  )
})

app.get('/api/:model/:id', ({ params: { model, id } }, res) => {
  const err = (notFound[model] = id)
  const index = data[model]

  if (!model) res.send(err)

  const record = find(index, record => record.id === parseInt(id)) || {
    error: `ID: ${id} not found on model: ${model}`,
  }

  res.send(record)
})

app.listen(port, () => console.log(`Express running on port ${port}`))
