import React, { useState, useEffect } from 'react'
import { filter, find } from 'lodash'
import axios from 'axios'

import './app.scss'

import { ID, List, User, Item, Essentials } from 'types'
import { UserSelect } from '~UserSelect'
import { ListSelect } from '~ListSelect'
import { ListView } from '~ListView'

export const App: React.FC = () => {
  // indexes
  const [users, setUsers] = useState<User[]>([])
  const [lists, setLists] = useState<List[]>([])

  const [user, setUser] = useState<User | null>()
  const [userLists, setUserLists] = useState<List[]>([])
  const [list, setList] = useState<List | null>()
  const [userEssentials, setUserEssentials] = useState<Item[]>()
  const [done, setDone] = useState(false)
  const [count, setCount] = useState(0)

  const qp = `?count=${count}`

  useEffect(() => {
    const getAll = async () => {
      setUsers((await axios.get<User[]>('/api/users' + qp)).data)
      setLists((await axios.get<List[]>('/api/lists' + qp)).data)
    }
    getAll()
  }, [count])

  const login = async (id: ID) => {
    setUser((await axios.get<User>(`/api/users/${id}`)).data)
    setUserEssentials(
      (await axios.get<Essentials>(`/api/essentials/${id}`)).data.items
    )
  }

  const selectList = (id: ID) => {
    setList({ ...find(lists, li => li.id === id) })
  }

  const reset = () => {
    setDone(true)
    setCount(count + 1)
    // so user can review
  }

  const init = () => {
    setDone(false)
    setUser(null)
    setUserLists([])
    setUserEssentials([])
    setList(null)
  }

  useEffect(() => {
    if (!user) return
    const getLists = async () => {
      setUserLists((await axios.get<List[]>(`/api/lists/${user.id}`)).data)
    }
    getLists()
  }, [user])

  if (done)
    return (
      <>
        <h1>{user.name}, You Did It!</h1>
        <h3>{list.items.length + userEssentials.length} things remembered!</h3>
        <button onClick={() => init()}>Start Over!</button>
      </>
    )

  return user ? (
    <>
      {!list && (
        <>
          <h1>{user.name}, WYD?</h1>
          {userLists.map(li => (
            <ListSelect
              key={li.id + li.name}
              list={li}
              selectList={selectList}
            />
          ))}
        </>
      )}
      {list && (
        <ListView reset={reset} list={list} essentials={userEssentials} />
      )}
    </>
  ) : (
    <div>
      <h1>Don't Forget Yer Shit!</h1>

      <h3>Who Dis?</h3>
      {users.map(user => (
        <UserSelect key={user.id + user.name} user={user} login={login} />
      ))}
    </div>
  )
}
