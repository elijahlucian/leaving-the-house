import React from 'react'
import { User, ID } from '~types'

type Props = {
  user: User
  login: (id: ID) => void
}

interface This {}

export const UserSelect: React.FC<Props> = ({ user: { id, name }, login }) => {
  const handleClick = () => {
    login(id)
  }

  return <button onClick={handleClick}>{name}</button>
}
