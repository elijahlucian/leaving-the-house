import React from 'react'
import { Item, ID } from '~types'

type Props = {
  i: number
  item: Item
  done?: boolean
  toggleItem: (id: ID) => void
}
export const ItemView: React.FC<Props> = ({ item, toggleItem, i }) => {
  const clicked = () => {
    toggleItem(i)
  }

  return (
    <button onClick={clicked} className={item.done ? 'done' : 'not-done'}>
      {item.name}
    </button>
  )
}
