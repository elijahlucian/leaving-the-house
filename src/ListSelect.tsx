import React from 'react'
import { ID, List } from '~types'

type Props = {
  selectList: (id: ID) => void
  list: List
}
export const ListSelect: React.FC<Props> = ({
  selectList,
  list: { id, name },
}) => {
  const handleClick = () => {
    selectList(id)
  }

  return <button onClick={handleClick}>{name}</button>
}
