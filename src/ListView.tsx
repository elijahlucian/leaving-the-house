import React, { useState, useEffect } from 'react'
import { filter } from 'lodash'

import { List, ID, Item } from '~types'
import { ItemView } from '~ItemView'

type Props = {
  list: List
  essentials: Item[]
  reset: () => void
}

export const ListView = ({ list, essentials, reset }: Props) => {
  const [items, setItems] = useState<Item[]>([]) // organize by index

  const toggleItem = (id: ID) => {
    const newItems = [...items]
    newItems[id].done = !newItems[id].done
    setItems(newItems)
  }

  useEffect(() => {
    if (!items.length) return
    if (filter(items, item => item.done).length == items.length) {
      setItems([])
      reset()
    }
  }, [items])

  useEffect(() => {
    const allItems = [...essentials, ...list.items]
    setItems(allItems)
  }, [list.id])

  return (
    <>
      <h1>{list.name}? Do the things!</h1>

      {items.map((item, i) => (
        <ItemView
          key={i + item.name}
          i={i}
          toggleItem={toggleItem}
          item={item}
        />
      ))}
    </>
  )
}
