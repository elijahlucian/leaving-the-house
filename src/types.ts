type ListTypes = 'leaving the house' | 'going to bed' | 'waking up'

export type ID = number

export type User = {
  id: ID
  name: string
}

export type Essentials = {
  user_id: ID
  items: Item[]
}

export type List = {
  id: ID
  user_id: User['id']
  name: string
  items: Item[]
  last_completed: number // timestamp for daily resets.
  created_at: number // timestamp
  type: ListTypes
}

export type Item = {
  name: string
  done?: boolean // Later: add a timestamp of when done - (X minutes ago...)
}
